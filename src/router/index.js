import Vue from "vue";
import VueRouter from "vue-router";
import Dash from "../views/Dash.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Dash",
    component: Dash
  }
];

const router = new VueRouter({
  routes
});

export default router;
